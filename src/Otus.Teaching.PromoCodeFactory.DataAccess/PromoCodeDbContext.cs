using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System.Reflection;

namespace Otus.Teaching.PromoCodeFactory.Core
{
    public class PromoCodeDbContext : DbContext
    {
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Preference> Preferences { get; set; }
        public DbSet<PromoCode> PromoCodes { get; set; }

        public PromoCodeDbContext()
        {

        }

        public PromoCodeDbContext(DbContextOptions<PromoCodeDbContext> options)
            : base(options)
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var connectionStringBuilder = new SqliteConnectionStringBuilder { DataSource = "PromoCodeDB.db" };
            var connectionString = connectionStringBuilder.ToString();
            var connection = new SqliteConnection(connectionString);

            optionsBuilder.UseSqlite(connection);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Employee>().ToTable("Employees");
            modelBuilder.Entity<Role>().ToTable("Roles");
            modelBuilder.Entity<Customer>().ToTable("Customers");
            modelBuilder.Entity<Preference>().ToTable("Preferences");
            modelBuilder.Entity<PromoCode>().ToTable("PromoCodes");

            modelBuilder.Entity<CustomerPreference>()
               .HasKey(cp => new { cp.CustomerId, cp.PreferenceId });

            modelBuilder.Entity<CustomerPreference>()
                .HasOne(cp => cp.Customer)
                .WithMany(b => b.CustomerPreferences)
                .HasForeignKey(bc => bc.CustomerId);

            modelBuilder.Entity<CustomerPreference>()
                .HasOne(cp => cp.Preference)
                .WithMany()
                .HasForeignKey(cp => cp.PreferenceId);

            modelBuilder.Entity<Customer>()
                .HasMany(c => c.PromoCodes)
                .WithOne(p => p.Customer);

            modelBuilder.Entity<Preference>()
                .HasMany(p => p.PromoCodes)
                .WithOne(pc => pc.Preference);

            modelBuilder.Entity<Employee>()
                .HasOne(e => e.Role)
                .WithMany()
                .HasForeignKey(e => e.RoleId);
        }
    }
}